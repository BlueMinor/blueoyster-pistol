package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/matryer/vice/queues/nsq"
	//"./limiter"
)

const WORKER_COUNT = 5
const JOB_COUNT = 100

var (
	wg sync.WaitGroup
)

type worker struct {
	Ctx    context.Context
	Cancel func()
}

func NewWorker() worker {
	context1, cancel1 := context.WithTimeout(context.Background(), 10*time.Minute)
	return worker{
		Ctx:    context1,
		Cancel: cancel1,
	}
}

func (w worker) Shoot(id int) error {
	tr := &http.Transport{}
	client := &http.Client{Transport: tr, Timeout: time.Duration(5 * time.Minute)}

	// anonymous struct to pack and unpack data in the channel
	c := make(chan struct {
		r   *http.Response
		err error
	}, 1)

	req, _ := http.NewRequest("GET", "http://localhost:3000/answer", nil)
	go func() {
		fmt.Println("Doing http request is a hard job")
		resp, err := client.Do(req)
		pack := struct {
			r   *http.Response
			err error
		}{resp, err}
		c <- pack
	}()

	select {
	case <-w.Ctx.Done():
		fmt.Println(fmt.Sprintf("Cancel the context %d", id))
		tr.CancelRequest(req)
		//<-c // Wait for client.Do
		fmt.Println(w.Ctx.Err())
		return w.Ctx.Err()
	case ok := <-c:
		fmt.Println(fmt.Sprintf("processed %d", id))
		err := ok.err
		resp := ok.r
		if err != nil {
			fmt.Println("Error ", err)
			return err
		}

		defer resp.Body.Close()
		out, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("Server Response: %s\n", out)
	}

	fmt.Println("out of select")
	return nil
}

func (w worker) Run(id int) {
	for {
		select {
		case <-w.Ctx.Done():
			log.Println("finished")
			return
		default:
			time.Sleep(1 * time.Second)
			log.Print(fmt.Sprintf("%d ...", id))
		}
	}
}

//type worker = func(id int, x context.Context) error

func setupTransport(workerMap map[int]worker) {
	ncontext := context.Background()

	ncontext, ncancel := context.WithTimeout(ncontext, 10*time.Minute)
	_, cancel := context.WithTimeout(context.Background(), 10*time.Minute)

	defer ncancel()

	transport := nsq.New()
	defer func() {
		transport.Stop()
		<-transport.Done()
	}()

	names := transport.Receive("names")
	greetings := transport.Send("greetings")

	go Greeter(workerMap, ncontext, func() { cancel() }, names, greetings, transport.ErrChan())
}

func main1() {
	pool := make(map[int]worker)

	fmt.Println("Hey, I'm going to do some work")

	//wg.Add(1)
	//go work(context)
	//go longWork(context)
	//cancel()
	//wg.Wait()

	pool[0] = NewWorker()
	pool[1] = NewWorker()

	ncontext := context.Background()

	ncontext, ncancel := context.WithTimeout(ncontext, 10*time.Minute)
	_, cancel := context.WithTimeout(context.Background(), 10*time.Minute)

	defer ncancel()

	transport := nsq.New()
	defer func() {
		transport.Stop()
		<-transport.Done()
	}()

	names := transport.Receive("names")
	greetings := transport.Send("greetings")

	go Greeter(pool, ncontext, func() { cancel() }, names, greetings, transport.ErrChan())

	for k, v := range pool {
		go v.Shoot(k)
	}

	fmt.Scanln()
	fmt.Println("Finished. I'm going home")
}

func FakeGreeter(workerMap map[int]worker, ctx context.Context, cancel func(), names <-chan []byte, greetings chan<- []byte, errs <-chan error) {

}

func Greeter(workerMap map[int]worker, ctx context.Context, cancel func(), names <-chan []byte, greetings chan<- []byte, errs <-chan error) {
	for {
		select {
		case <-ctx.Done():
			log.Println("finished")
			return
		case err := <-errs:
			log.Println("an error occurred:", err)
		case name := <-names:
			fmt.Println("Cancel Http")
			value, _ := strconv.Atoi(string(name))
			fmt.Println(value)
			workerMap[value].Cancel()
			//cancel()
			//greeting := "Hello " + string(name)
			//greetings <- []byte(greeting)
		}
	}
}

func longWork(id int, ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			log.Println("finished")
			return nil
		default:
			time.Sleep(1 * time.Second)
			log.Print(fmt.Sprintf("%d ...", id))
		}
	}
}

func work(ctx context.Context) error {
	//defer wg.Done()

	tr := &http.Transport{}
	client := &http.Client{Transport: tr, Timeout: time.Duration(5 * time.Minute)}

	// anonymous struct to pack and unpack data in the channel
	c := make(chan struct {
		r   *http.Response
		err error
	}, 1)

	req, _ := http.NewRequest("GET", "http://localhost:3000/answer", nil)
	go func() {
		fmt.Println("Doing http request is a hard job")
		resp, err := client.Do(req)
		pack := struct {
			r   *http.Response
			err error
		}{resp, err}
		c <- pack
	}()

	select {
	case <-ctx.Done():
		fmt.Println("Cancel the context")
		tr.CancelRequest(req)
		//<-c // Wait for client.Do
		fmt.Println(ctx.Err())
		return ctx.Err()
	case ok := <-c:
		err := ok.err
		resp := ok.r
		if err != nil {
			fmt.Println("Error ", err)
			return err
		}

		defer resp.Body.Close()
		out, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("Server Response: %s\n", out)
	}

	fmt.Println("out of select")

	return nil
}
